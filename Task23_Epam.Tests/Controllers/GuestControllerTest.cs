﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Task23_Epam;
using Task23_Epam.Controllers;

namespace Task23_Epam.Tests.Controllers
{
    [TestClass]
    public class GuestControllerTest
    {
        [TestMethod]
        public void StartHttpGetGuest_ExpectNotNullResult()
        {
            // Arrange
            GuestController controller = new GuestController();

            // Act
            ViewResult result = controller.Guest() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void PostGuest_WriteComment_ExpectNotNullResult()
        {
            // Arrange
            GuestController controller = new GuestController();

            // Act
            ViewResult result = controller.Guest("Vova", "Volodya") as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void PostGuest_WriteComment_ExpectCountCommentIncreaseInOne()
        {
            // Arrange
            GuestController controller = new GuestController();
            ViewResult start = controller.Guest("Vova", "Volodya") as ViewResult;
            var expected = start.ViewBag.Comments.Count + 1;

            // Act
            ViewResult result = controller.Guest("Vova", "Volodya") as ViewResult;
            var actual = result.ViewBag.Comments.Count;

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
